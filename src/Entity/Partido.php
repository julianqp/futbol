<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="movie")
 */
class Partido implements \JsonSerializable {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     *
     */
    private $ano;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $jornada;
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     *
     */
    private $local;
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     *
     */
    private $visitante;
    /**
    * @ORM\Column(type="integer")
    * @Assert\NotBlank()
     *
    */
    private $gl1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $gl2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $gv1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $gv2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $cl1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $cl2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $cv1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $cv2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $tl1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $tl2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $tv1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     *
     */
    private $tv2;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano): void
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getJornada()
    {
        return $this->jornada;
    }

    /**
     * @param mixed $jornada
     */
    public function setJornada($jornada): void
    {
        $this->jornada = $jornada;
    }

    /**
     * @return mixed
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param mixed $local
     */
    public function setLocal($local): void
    {
        $this->local = $local;
    }

    /**
     * @return mixed
     */
    public function getVisitante()
    {
        return $this->visitante;
    }

    /**
     * @param mixed $visitante
     */
    public function setVisitante($visitante): void
    {
        $this->visitante = $visitante;
    }

    /**
     * @return mixed
     */
    public function getGl1()
    {
        return $this->gl1;
    }

    /**
     * @param mixed $gl1
     */
    public function setGl1($gl1): void
    {
        $this->gl1 = $gl1;
    }

    /**
     * @return mixed
     */
    public function getGl2()
    {
        return $this->gl2;
    }

    /**
     * @param mixed $gl2
     */
    public function setGl2($gl2): void
    {
        $this->gl2 = $gl2;
    }

    /**
     * @return mixed
     */
    public function getGv1()
    {
        return $this->gv1;
    }

    /**
     * @param mixed $gv1
     */
    public function setGv1($gv1): void
    {
        $this->gv1 = $gv1;
    }

    /**
     * @return mixed
     */
    public function getGv2()
    {
        return $this->gv2;
    }

    /**
     * @param mixed $gv2
     */
    public function setGv2($gv2): void
    {
        $this->gv2 = $gv2;
    }

    /**
     * @return mixed
     */
    public function getCl1()
    {
        return $this->cl1;
    }

    /**
     * @param mixed $cl1
     */
    public function setCl1($cl1): void
    {
        $this->cl1 = $cl1;
    }

    /**
     * @return mixed
     */
    public function getCl2()
    {
        return $this->cl2;
    }

    /**
     * @param mixed $cl2
     */
    public function setCl2($cl2): void
    {
        $this->cl2 = $cl2;
    }

    /**
     * @return mixed
     */
    public function getCv1()
    {
        return $this->cv1;
    }

    /**
     * @param mixed $cv1
     */
    public function setCv1($cv1): void
    {
        $this->cv1 = $cv1;
    }

    /**
     * @return mixed
     */
    public function getCv2()
    {
        return $this->cv2;
    }

    /**
     * @param mixed $cv2
     */
    public function setCv2($cv2): void
    {
        $this->cv2 = $cv2;
    }

    /**
     * @return mixed
     */
    public function getTl1()
    {
        return $this->tl1;
    }

    /**
     * @param mixed $tl1
     */
    public function setTl1($tl1): void
    {
        $this->tl1 = $tl1;
    }

    /**
     * @return mixed
     */
    public function getTl2()
    {
        return $this->tl2;
    }

    /**
     * @param mixed $tl2
     */
    public function setTl2($tl2): void
    {
        $this->tl2 = $tl2;
    }

    /**
     * @return mixed
     */
    public function getTv1()
    {
        return $this->tv1;
    }

    /**
     * @param mixed $tv1
     */
    public function setTv1($tv1): void
    {
        $this->tv1 = $tv1;
    }

    /**
     * @return mixed
     */
    public function getTv2()
    {
        return $this->tv2;
    }

    /**
     * @param mixed $tv2
     */
    public function setTv2($tv2): void
    {
        $this->tv2 = $tv2;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'ano' => $this->getAno(),
            'jornada' => $this->getJornada(),
            'local' => $this->getLocal(),
            'visitante' => $this->getVisitante(),
            'gl1' => $this->getGl1(),
            'gl2' => $this->getGl2(),
            'gv1' => $this->getGv1(),
            'gv2' => $this->getGv2(),
            'cl1' => $this->getCl1(),
            'cl2' => $this->getCl2(),
            'cv1' => $this->getCv1(),
            'cv2' => $this->getCv2(),
            'tl1' => $this->getTl1(),
            'tl2' => $this->getTl2(),
            'tv1' => $this->getTv1(),
            'tv2' => $this->getTv2(),
        ];
    }
}