<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Movie;
use App\Service\API;

/**
 * Mensaje controller.
 * @Route("/api", name="api_")
 */
class MovieController extends Controller
{
    /**
     * Lists all Mensajes.
     * @Route("/peliculas",
     *     methods = {"GET"},
     *     name = "getMovies")
     *
     * @return Response
     */
    public function getMovies()
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        $mensaje = $repository->findall();
        return API::send_data($mensaje);
    }

    /**
     *  Info de una tarea.
     * @Route("/peliculas/{id}", methods={"GET"})
     * @param String id
     * @return Response
     */
    public function getMovie($id)
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        $mensaje = $repository->findBy(['$id' => $id]);
        return API::send_data($mensaje);
    }

    /**
     *  Creating a new user.
     * @Route("/peliculas",
     *     methods={"POST"},
     *     name = "postMovie")
     *
     * @return Response
     */
    public function postMovie(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $manager = $this->getDoctrine()->getManager();

        $mensaje = new Movie();
        $mensaje->setName($data['name']);
        $mensaje->setDescription($data['description']);

        $manager->persist($mensaje);
        $manager->flush();
        return API::send_data([]);
    }
}
