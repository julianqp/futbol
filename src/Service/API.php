<?php

namespace App\Service;
use Symfony\Component\HttpFoundation\Response;

class API
{

    /**
     * @param $item
     * @return Response
     */
    public static function send_data($data) {

        $response = [];
        $response['data'] = !is_null($data) ? $data : array();
        $cabecera = !is_null($data) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;

        $response = new Response(
            json_encode($response), //datos
            $cabecera,                 // cabecera HTTP
            array('content-type' => 'application/json') // Tipo de contenido
        );

        $response->headers->set('Access-Control-Allow-Origin', '*');        //cabecera para evitar CORS
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, DELETE'); // Metodos que aceptas.

        return $response;
    }

}